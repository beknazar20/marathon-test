import Header from './components/header'
import Information from './components/information'
import News from './components/news'
import './assets/styles/app.css'
function App() {
  const information = [
    {
      id:1,
        title: 'Хакатон трёх городов',
        description: 'Хакатон для молодых аналитиков и разработчиков.Постройте оптимальный путь по сложной поверхности',
        date: '24–25 сентября',
        location:'Уфа, Самара и Казань',
        prize:'Призовой фонд — 289 000 ₽'
    },
    {
      id:2,
      title: 'Хакатон трёх городов ',
      description: 'Хакатон для молодых аналитиков и разработчиков.Постройте оптимальный путь по сложной поверхности',
      date: '24–25 сентября',
      location:'Уфа, Самара и Казань',
      prize:'Призовой фонд — 289 000 ₽'
  },
]
  return (
    <div className="App">
      <Header/>
      <div className="content-wrapper">
         <News/>
        <Information informations={information}/>
      </div>
      
    </div>
  );
}

export default App;
