import React , { Component }from 'react';
import '../assets/styles/header.css'
export default class Header extends Component {

    render(){
        return (
            <div className="header">
                <div className="logo">
                    <img src="images/rosneft-logo.png" alt="Logof"/>
                </div> 
                <ul className="menu-list">
                    <li className="menu-items"><a href="/">Главная</a></li>
                    <li className="menu-items"><a href="/">Организаторы</a></li>
                    <li className="menu-items"><a href="/">Правила</a></li>
                    <li className="menu-items"><a href="/">#пулсьМарафон</a></li>
                    <li className="menu-items"><a href="/">rn.digital</a></li>
                </ul>
            </div>
        )
    }

}