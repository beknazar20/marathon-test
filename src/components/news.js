import React , { Component }from 'react';
import  '../assets/styles/news.css'

export default class News extends Component {

    
    render(){
        return (
            <div className="news">
            <div className="title-wrap">
            <h2 className="bolock-title">Марафон <br/> ИТ-соревнований
             </h2>
             <img src="images/2020.png" alt="2020" className="img2020" />
                </div>  
               <p className="news-description">
                   Роснефть приглашает разработчиков и аналитиков принять участие в одном из трёх соревнований
                </p>

                <div className="news-content">
                        <h3 className="pulse-marathon">#ПульсМарафон</h3>
                        <div className="short-news">
                            <h4 className="new-title">Отложение высвобождает пегматитовый сталагмит</h4>
                            <p className="new-content">Базис эрозии, основываясь большей частью на сейсмических 
                            данных, глобален. Эоловое засоление ослабляет комплекс. Лагуна, так же, как и в других...</p>
                            <a href="/" className="read-more">Читать дальше</a>
                        </div>


                        <div className="short-news">
                            <h4 className="new-title">Плато смещает аллит, что, <br/>однако, не уничтожило  <br/>доледниковую</h4>
                            <p className="new-content">Пока магма остается в камере, углефикация сдвигает меловой мусковит. 
                            Вулканическое стекло, скажем, за 100 тысяч лет, сменяет глетчерный приток, причем, вероятно...</p>
                            <a href="/" className="read-more">Читать дальше</a>
                        </div>
                            <button className="news-button">Ко всем новостям</button>
                            <div className="news-footer">
                                <p className="contacts">По всем вопросам:</p>
                                <p className="contacts">RD.knpk@bnipi.rosneft.ru</p>
                                <div className="social-icons">
                                    <img className="social-item" alt="vk"  src="images/vk.png" />
                                    <img className="social-item" alt="fackebook" src="images/fackebook.png" />
                                    <img className="social-item"  alt="instagram"src="images/instagram.png" />
                                </div>
                            </div>
                </div> 
            </div>
        )
    }

}