import React , { Component }from 'react';
import  '../assets/styles/information.css'

export default class Information extends Component {

 
    render(){
        const {informations} = this.props;
        const elements = informations.map(({id,title,description, date, location, prize}) => {
            return (<div key={id} className="information">
               < img src="images/information-logo-1.png" alt="information-logo" className="information-logo"/>
               <div className="information-content">
                   <h3 className="title">{title}</h3>
                   <p className="description">
                     {description}
                   </p>
                   <div className="info-box">
                   <div className="info-box-item">
                        <img src="/images/date-icon.png" alt="date-icon"/>
                        <span className="date-info-title">{date}</span> 
                   </div>
                   <div className="info-box-item">
                        <img src="/images/location-icon.png" alt="location-icon" />
                        <span className="date-info-title">{location}</span> 
                   </div>
                   <div className="info-box-item">
                        <img src="/images/prize-icon.png" className="info-img" alt="prize-icon" />
                        <span className="date-info-title">{prize}</span> 
                   </div>
                   </div>
                   <button className="info-btn">Подробнее</button>
               </div>
                <img src="/images/news-img-1.png" alt="information image" />
            </div>)
        })
        return (
            <div>
                {elements}
            </div>
        )
    }

}